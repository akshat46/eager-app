package com.pk.eager;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pk.eager.model.EmergencyData;
import com.squareup.picasso.Picasso;
import com.thefinestartist.finestwebview.FinestWebView;

public class DetailedViewActivity extends AppCompatActivity {

    EmergencyData data;
    private int position;
    private TextView text,publishedBy,date;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_view);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        data=getIntent().getExtras().getParcelable("data");
        position=getIntent().getExtras().getInt("position");

        text=findViewById(R.id.text);
        publishedBy=findViewById(R.id.published_by);
        date=findViewById(R.id.date);
        image=findViewById(R.id.image);

        text.setText(data.getArticles().get(position).getText());
        date.setText(data.getArticles().get(position).getPublished().split(" ")[0]);
        publishedBy.setText(data.getArticles().get(position).getTitle());



        findViewById(R.id.article_Button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new FinestWebView.Builder(DetailedViewActivity.this).show(data.getArticles().get(position).getLink());
            }
        });
    }
}
