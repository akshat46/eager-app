package com.pk.eager.ReportFragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pk.eager.R;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.SupportMapFragment;

import static android.app.Activity.RESULT_OK;


public class CustomLocation extends Fragment implements OnMapReadyCallback {
    private PlaceAutocompleteFragment autocompleteFragment;
    private SupportMapFragment mapFragment;
    private GoogleMap gMap;
    boolean marker = true;
    private LatLng location;
    FloatingActionButton done;
    private final int FRAGMENT_CODE= 2; //2: Fragment CustomLocation
    View view;

//    @Override
//    public void onCreate (Bundle savedInstanceState){
//        super.onCreate(savedInstanceState);
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(view==null){
            view = inflater.inflate(R.layout.fragment_search_location, container, false);
        }
        mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        done = (FloatingActionButton) view.findViewById(R.id.fab_done);

        autocompleteFragment = (PlaceAutocompleteFragment)getActivity().getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                System.out.println("Place: " + place.getLatLng());

                CameraUpdate center=
                        CameraUpdateFactory.newLatLngZoom(new LatLng(place.getLatLng().latitude, place.getLatLng().longitude), 15);
                if(CustomLocation.this.gMap != null){
                    gMap.clear();
                    gMap.animateCamera(center);
                    Marker marker = gMap.addMarker(new MarkerOptions().position(place.getLatLng()).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker)));
                    gMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
                        @Override
                        public void onCameraMove() {
                            // Get the center of the Map.
                            LatLng centerOfMap = gMap.getCameraPosition().target;
                            location = centerOfMap;
                            // Update your Marker's position to the center of the Map.
                            marker.setPosition(centerOfMap);
                        }
                    });
                }
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                System.out.println("An error occurred: " + status);
            }
        });
        done.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
//                    getActivity().getFragmentManager().beginTransaction().add(dialogFrag, "dialog").commit();
//                }else{
//                    getChildFragmentManager().beginTransaction().add(dialogFrag,"dialog").commit();
//                }
                Intent intent = new Intent(CustomLocation.this.getActivity(), CustomLocation.class);
                if(gMap!=null){
                    location = gMap.getCameraPosition().target;
                    intent.putExtra("lat", location.latitude);
                    intent.putExtra("lng", location.longitude);
                    intent.putExtra("filled", true);
                    Toast.makeText(getActivity(), "Location Selected",
                            Toast.LENGTH_LONG).show();
                }
                else{
                    intent.putExtra("filled", false);
                    Toast.makeText(getActivity(), "Location Not Selected",
                            Toast.LENGTH_LONG).show();
                }
                getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent);
                getFragmentManager().popBackStack();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap){
        this.gMap = googleMap;
    }

}
