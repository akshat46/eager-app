package com.pk.eager;

import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pk.eager.BaseClass.BaseXBeeFragment;

import org.w3c.dom.Text;

public class TabFragment extends BaseXBeeFragment {

    FragmentTabHost tabHost;


    public TabFragment() {
        // Required empty public constructor
    }

    public static TabFragment newInstance(String param1, String param2) {
        TabFragment fragment = new TabFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tab, container, false);
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        tabHost = (FragmentTabHost)getView().findViewById(android.R.id.tabhost);
        tabHost.setup(getActivity().getApplicationContext(), getActivity().getSupportFragmentManager(), android.R.id.tabcontent);

        View listViewTabIndicator = LayoutInflater.from(getActivity()).inflate(R.layout.tab_listview_indicator,
                tabHost.getTabWidget(), false);

        tabHost.addTab(
                tabHost.newTabSpec("List View").setIndicator(listViewTabIndicator),
                InformationListView.class, null);

        View mapViewTabIndicator = LayoutInflater.from(getActivity()).inflate(R.layout.tab_mapview_indicator,
                tabHost.getTabWidget(), false);
        tabHost.addTab(
                tabHost.newTabSpec("Map View").setIndicator(mapViewTabIndicator),
                Information.class, null);
        //mapViewTabIndicator.findViewById(R.id.tab_mapview).setAlpha(0.6f);
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String arg0) {
                View t1 = tabHost.getTabWidget().getChildTabViewAt(tabHost.getCurrentTab());
                TextView tv1 = (TextView)t1.findViewById(android.R.id.title);
                tv1.setAlpha(1);
                ImageView ic1 = (ImageView)t1.findViewById(android.R.id.icon);
                ic1.setAlpha(1f);

                TextView tv2 = (TextView)tabHost.getTabWidget().getChildTabViewAt(1 - tabHost.getCurrentTab()).findViewById(android.R.id.title);
                tv2.setAlpha(0.6f);
                ImageView ic2 = (ImageView)tabHost.getTabWidget().getChildTabViewAt(1 - tabHost.getCurrentTab()).findViewById(android.R.id.icon);
                ic2.setAlpha(0.6f);
                if(tabHost.getCurrentTab()==1){
                    getView().findViewById(R.id.tab_curve).setVisibility(View.GONE);
                }else{
                    getView().findViewById(R.id.tab_curve).setVisibility(View.VISIBLE);
                }
            }
        });

    }
}