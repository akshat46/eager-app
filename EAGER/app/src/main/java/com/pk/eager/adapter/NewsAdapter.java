package com.pk.eager.adapter;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pk.eager.DetailedViewActivity;
import com.pk.eager.R;
import com.pk.eager.model.EmergencyData;
import com.squareup.picasso.Picasso;
import com.thefinestartist.finestwebview.FinestWebView;


public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.Holder> {

    Context context;
    EmergencyData data;
    View itemview;

    public NewsAdapter(Context context, EmergencyData data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        itemview= LayoutInflater.from(context).inflate(R.layout.adapter,viewGroup,false);
        return new Holder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        holder.title.setText(data.getArticles().get(i).getText());
        holder.published.setText(data.getArticles().get(i).getPublished().split(" ")[0]);
        holder.profile_title.setText(data.getArticles().get(i).getTitle());
        if(data.getArticles().get(i).getImage() != null && !data.getArticles().get(i).getImage().isEmpty()){
            holder.image_ll.setVisibility(View.VISIBLE);
            //((LinearLayout.LayoutParams)holder.description_ll.getLayoutParams()).weight = 7;
            holder.large_image.setVisibility(View.VISIBLE);
            Picasso.get().load(data.getArticles().get(i).getImage()).placeholder(R.drawable.default_news).into(holder.large_image);
        }
    }

    @Override
    public int getItemCount() {
        int count=data.getArticles().size();
        return count;
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView title,published,profile_title;
        ImageView image, large_image;
        LinearLayout description_ll;
        CardView image_ll;
        public Holder(@NonNull View itemView) {
            super(itemView);
            title=itemView.findViewById(R.id.title);
            published=itemView.findViewById(R.id.published);
            profile_title=itemView.findViewById(R.id.profile_title);
            image_ll = itemView.findViewById(R.id.image_ll);
            large_image = image=itemView.findViewById(R.id.image_large);
            //TODO: change onclick listener. direct to web view
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    new FinestWebView.Builder(context).show(data.getArticles().get(getAdapterPosition()).getLink());
                    context.startActivity(new Intent(context, DetailedViewActivity.class).putExtra("data",data).putExtra("position",getAdapterPosition()));
                }
            });
        }
    }
}
