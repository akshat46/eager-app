
package com.pk.eager.model;


import android.os.Parcel;
import android.os.Parcelable;

public class Article implements Parcelable {


    private String link = "";

    private String published = "";

    private String text = "";

    private String title = "";
    private String image = "";

    public Article(){

    }


    public Article(String link, String published, String text, String title, String image) {
        this.link = link;
        this.published = published;
        this.text = text;
        this.title = title;
        this.image = image;
    }

    protected Article(Parcel in) {
        link = in.readString();
        published = in.readString();
        text = in.readString();
        title = in.readString();
        image = in.readString();
    }

    public static final Creator<Article> CREATOR = new Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel in) {
            return new Article(in);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(link);
        parcel.writeString(published);
        parcel.writeString(text);
        parcel.writeString(title);
        parcel.writeString(image);
    }
}
