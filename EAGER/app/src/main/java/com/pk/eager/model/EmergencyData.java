
package com.pk.eager.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class EmergencyData implements Parcelable{


    private List<Article> articles = null;

    protected EmergencyData(Parcel in) {
        articles = in.createTypedArrayList(Article.CREATOR);
    }

    public static final Creator<EmergencyData> CREATOR = new Creator<EmergencyData>() {
        @Override
        public EmergencyData createFromParcel(Parcel in) {
            return new EmergencyData(in);
        }

        @Override
        public EmergencyData[] newArray(int size) {
            return new EmergencyData[size];
        }
    };

    public List<Article> getArticles() {
        return articles;
    }

    public EmergencyData(){

    }
    public EmergencyData(List<Article> articles) {
        this.articles = articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(articles);
    }
}
